<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
      Welcome to Emmet's Clothing! Where you can find designer fashion for the entire family. When we welcomed our first baby girl into the world we found so many adorable outfits we wanted her to wear! But as we started looking we found out the winter dresses weren't winter dresses and were covered up by cardigans or were way shorter than we wanted. It was apparent that shopping for our baby girl would not be as easy as we thought and would only get harder!
    </p>
    <p>
      Emmet's was founded with a desire to create designer clothing that would be adorable and meet our daughter's needs instead of cutting corners and accessorizing to increase profits and cut costs! As we talked with other parents we saw a need for more choices and cuter options for baby boys clothing. There just wasn't as much options for mommy's wanting to give their little boy the swag they wanted. We started by offering a designer white dress shirts that looked as elegant as it was supposed to without the excess seams other companies used to improve mass production. And thus Emmet's was born with clothing that proved to be Desinger, Affordable and Adorable.
    </p>
    <p>
      Emmet's was born with a vision to improve the infant's clothing business and eventually offer products for the entire family. We work hard and have fun at what we do. Trent handles the manufacturing and web design. While Emily assists in the clothing designs, patterns and styles. She adds the woman's touch needed to reach our customers needs and so much more. The kids help where they can too, trying on outfits, modeling, organizing where possible and always providing us laughs and smiles.
    </p>
    <p>
        Emmet's - Designer, Affordable, Adorable
    </p>
    <p>
        - The Nielson Family
    </p>
</div>
